/*
 * @Author: BuddyCoder
 * @Date: 2021-08-13 16:42:05
 * @LastEditors: BuddyCoder
 * @LastEditTime: 2022-09-20 21:34:48
 * @Description: 
 * @FilePath: /pgmacro/src/lib.rs
 * MIT
 */
extern crate proc_macro; //不管要不要都写了
use proc_macro::TokenStream;
use syn::{parse_macro_input, DeriveInput};
use syn;

mod macros;
use crate::macros::crud_table_impl::{impl_crud_driver};

//增加(Create)、读取查询(Retrieve)、更新(Update)和删除(Delete)
#[proc_macro_derive(PGCRUD)]
pub fn derive_builder(input: TokenStream) -> TokenStream { 
    let derive_input = parse_macro_input!(input as DeriveInput);
    let result = match derive_input.data {
        syn::Data::Struct(ref data_struct) => impl_crud_driver(&derive_input, &data_struct.fields),
        _ => panic!("doesn't work with unions yet"),
    };
    result.into()
}





